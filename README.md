
Naitive dependencies of [imgflo](http://github.com/jonnor/imgflo)
and build-system for these.

Releases before version 50 were for cedar Heroku stack,
later are for cedar-14.

License
--------
See the individual git submodules for the licenses of the projects used.
Patches have the same license as the project they are for.

Anything else may be used under the MIT license.
